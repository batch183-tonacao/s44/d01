// fetch() method in JavaScript is used to send request in the server and load the received response in the webpages. The request and response is in JSON format
/*
	syntax:
		fetch('url', options);

*/

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response)=> response.json())
// .then((data)=>console.log(data));
.then((data)=> showPosts(data));


//Add post data.
// This will trigger an event that will add a new post in our mock database upon clicking the "Create" button.
document.querySelector("#form-add-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts",{
		method: "POST",
		body: JSON.stringify({
			userId: 1,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		}),
		headers:{
			"Content-type": "application/json"
		}
	})
	.then((response)=> response.json())
	.then((data)=> showPosts(data));
	console.log(data);
	alert("Successfully added!");

	// resets the state of our input
	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;
})


// View Posts
const showPosts = (posts) =>{
	let postEntries = "";
	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}
/*
// Edit Post Button
// We will create a function that will be called in the onclick() event and will pass  the value in the Update form input box

const editPost = (id) =>{
	// contain the value of the title and body in a variable
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	// Pass the id, title, and body of the movie post to be updated in the Edit POST/FORM
	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

}

// Update post.
// This will trigger an event that will update a post upon clicking the Update button.
document.querySelector("#form-edit-post").addEventListener("submit", (e) =>{
	e.preventDefault();
	let postId = document.querySelector("#txt-edit-id").value
	let title = document.querySelector("#txt-edit-title").value
	let body = document.querySelector("#txt-edit-body").value

	console.log(postId);

	// Use a loop that will check each post stored in our mock database.
	for(i = 0; i < posts.length; i++){
		//Use a if statement to look for the post to be updated using id property.
		// The value posts[i].id is a Number while postId is a String, if we want to use the strictly equality operator (===) we need to convert the postId first.
		if(posts[i].id == postId){
			// reassign the value of the title and body property.
			posts[i].title = title;
			posts[i].body = body;

			// Invoke the showPosts() to check the updated posts.
			showPosts();

			alert("Successfully updated!");

			break;
		}
	}
})

//Activity Delete a post
const deletePost = (id) =>{
	for(i=0; i<posts.length; i++){
		if(posts[i].id == id){
			posts.splice(i, 1);
		}
	}
	showPosts();
}

*/